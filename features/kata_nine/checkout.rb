# language: pt

require_relative './desconto.rb'
require_relative './politica_desconto.rb'

class CheckOut
  def initialize(regras)
    @regras = regras
    @produtos = {}
  end

  def scan(produto)
    @produtos[produto] || @produtos[produto] = 0
    @produtos[produto] =  @produtos[produto] + 1
  end

  def total
    @produtos.inject(0) do |mapeado, (produto, quantidade)|
      mapeado + preco_por(produto, quantidade)
    end
  end

  def preco_por(produto, quantidade)
    if regra_por(produto)
      regra_por(produto).preco_por(quantidade)
    else
      puts "Produto '#{produto}' escaneado é inválido"
    end
  end
  puts 'produto'

  def regra_por(produto)
    @regras[produto]
  end
end
