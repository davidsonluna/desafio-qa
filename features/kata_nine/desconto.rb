# language: pt

class Desconto
  def initialize(valor, quantidade)
    @valor = valor
    @quantidade = quantidade
  end

  def calculado(quantidade)
    (quantidade / @quantidade).floor * @valor
  end
end
