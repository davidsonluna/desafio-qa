# language:pt

class PoliticaDesconto
  def initialize(valor_inicial, * descontos)
    @valor_inicial = valor_inicial
    @descontos = descontos
  end

  def preco_por(quantidade)
    quantidade * @valor_inicial - desconto_por(quantidade)
  end

  def desconto_por(quantidade)
    @descontos.inject(0) do |mapeado, desconto|
      mapeado + desconto.calculado(quantidade)
    end
  end
end

RULES = {
  'A' => PoliticaDesconto.new(50, Desconto.new(20, 3)),
  'B' => PoliticaDesconto.new(30, Desconto.new(15, 2)),
  'C' => PoliticaDesconto.new(20),
  'D' => PoliticaDesconto.new(15)
}
