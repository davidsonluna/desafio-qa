#language:pt

Funcionalidade: Escrever mensagem 

    Para que eu possa escrever mensagem
    Sendo um usuário cadastrado 
    Posso escrever mensagem para meus contatos

    Contexto: Buscar usuário por nome existente nos contatos
        Dado que acesso meu perfil
        E realizo busca por '❤Amor'
        Então devo ver listados todos os chat '❤Amor'

    @message  
    Cenário: Escrever mensagem
        Quando digito mensagem 'Olá Amor!'
        Então a mensagem deve ser enviada
        
        
             



    
