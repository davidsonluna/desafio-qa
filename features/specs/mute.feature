#language:pt

Funcionalidade: Deixar chat com status Mute 

    Para que eu deixar um chat com status mute
    Sendo um usuário cadastrado 
    Posso acionar a opção mute de qualquer chat

    Contexto: Buscar usuário por nome existente nos contatos
        Dado que acesso meu perfil
        E realizo busca por '❤Amor'
        Então devo ver listados todos os chat '❤Amor'

    @mute_unmute 
    Cenário: Deixar chat com status Mute 
        Quando clico no botão do mute no menu
        Então então deve haver a mensagem 'Chat muted'
        
    @mute_unmute    
    Cenário: Deixar chat com status Unmute 
        Quando clico no botão do ummute no menu
        Então então deve haver a mensagem 'Chat unmuted'
        
             



    
