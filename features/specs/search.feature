#language:pt

Funcionalidade: Busca de usuário por palavra chave

    Para que eu possa buscar usuários por palavra chave
    Sendo um usuário cadastrado 
    Posso ver todos os usuários listados por palavra chave buscada

    @busca    
    Cenário: Buscar usuário por nome existente nos contatos
        Dado que acesso meu perfil
        Quando realizo busca por 'Wanderley'
        Então devo ver listados todos os chat 'Wanderley'
        
    Cenário: Buscar usuário por nome não existente nos contatos
        Dado que acesso meu perfil
        Quando realizo busca por 'U.R.S.A.L'
        Então devo ver a seguinte mensagem  "No chats, contacts or messages found"
        
             



    
