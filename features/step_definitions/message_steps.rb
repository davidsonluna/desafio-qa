Quando('digito mensagem {string}') do |message|
  @message.message(message)
  @message.send_msg
end

Então('a mensagem deve ser enviada') do
  expect(page).to have_content('Olá Amor!')
end