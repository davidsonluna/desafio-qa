Quando('clico no botão do mute no menu') do
  @navbar.mute_chat
end

Então('então deve haver a mensagem {string}') do |message|
  expect(page).to have_content message
end

Quando('clico no botão do ummute no menu') do
  @navbar.unmute_chat
end
