Dado('que acesso meu perfil') do
  @home_page.url
end

Quando('realizo busca por {string}') do |name|
  @home_page.search(name)
end

Então('devo ver listados todos os chat {string}') do |title|
  expect(@home_page.user).to have_content title
end

Então('devo ver a seguinte mensagem  {string}') do |message|
  expect(page).to have_content message
end
