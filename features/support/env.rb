require 'capybara'
require 'capybara/cucumber'

@env = ENV['BROWSER']

Capybara.configure do |config|
  if @env.eql?('chrome')
    config.default_driver = :selenium_chrome
  elsif @env.eql?('firefox')
    config.default_driver = :selenium
  elsif @env.eql?('headless')
    config.default_driver = :selenium_chrome_headless
  end
end

Capybara.default_max_wait_time = 30
