class Message
  include Capybara::DSL

  def message(message)
    find('*._2S1VP').set message
  end

  def send_msg
    find('*._35EW6').click
  end
end
