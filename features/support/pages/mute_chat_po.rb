class NavBar
  include Capybara::DSL

  def mute_chat
    find(:xpath, "/html//div[@id='main']//div[@class='_3Kxus']/div[3]").click
    find(:xpath, "//div[@title='Mute']").click
    find(:xpath, "//ol//li[1]//label[1]").click
    find(:xpath, "//div[@class='_1WZqU PNlAR']").click
  end

  def unmute_chat
    find(:xpath, "/html//div[@id='main']//div[@class='_3Kxus']/div[3]").click
    find(:xpath, "//div[@title='Cancel mute']").click
  end
end
