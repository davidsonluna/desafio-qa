class HomePage
  include Capybara::DSL

  def url
    visit 'https://web.whatsapp.com/'
  end

  def search(name)
    find('*.selectable-text').set name
  end

  def user
    find('*._3TEwt').click
  end
end
